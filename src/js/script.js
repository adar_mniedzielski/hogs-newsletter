$(function () {
	var e = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	$("#form").on("submit", function (t) {
		if (t.preventDefault(), !e.test($("input[name=email]").val())) return void $("input[name=email]").addClass("invalid");
		var n = {
			message: $("input[name=email]").val()
		};
		$.ajax({
			url: "http://hogs.pl/send-message.php",
			type: "post",
			data: n,
			success: function (e) {
				$("input[name=email]").removeClass("invalid"), $(".success").show(), $("input[name=email]").val("")
			},
			error: function (e) {
				$("input[name=email]").removeClass("invalid"), $(".error__text").text("CoĹ poszĹo nie tak... SprĂłbuj jeszcze raz."), $(".error").show()
			}
		})
	}), $("input[name=email]").on("change", function () {
		!e.test($("input[name=email]").val()) && $("input[name=email]").val() ? ($("input[name=email]").addClass("invalid"), $(".error__text").text("BĹÄdny adres email! SprĂłbuj jeszcze raz."), $(".error").show()) : ($(".error").hide(), $("input[name=email]").removeClass("invalid"))
	})
});